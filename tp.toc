\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}%
\contentsline {section}{\numberline {2}Electroweak Interactions and Spontaneous Symmetry Breaking}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Higgs Mechanism}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}The Glashow-Weinberg-Salam (GWS) theory of Electroweak Interactions}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}The Higgs Boson}{5}{subsection.2.3}%
\contentsline {section}{\numberline {3}The Large Hadron Collider (LHC)}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}The ATLAS Detector}{6}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Inner Detector}{6}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Calorimeters}{6}{subsubsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.3}Muon Spectrometer}{7}{subsubsection.3.1.3}%
\contentsline {subsubsection}{\numberline {3.1.4}Magnet System}{7}{subsubsection.3.1.4}%
\contentsline {subsection}{\numberline {3.2}The CMS Detector}{7}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Inner Tracker}{8}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}The Electromagnetic Calorimeter}{8}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}The Hadron Calorimeter}{8}{subsubsection.3.2.3}%
\contentsline {subsubsection}{\numberline {3.2.4}Muon System}{8}{subsubsection.3.2.4}%
\contentsline {section}{\numberline {4}Properties of the Higgs Boson}{9}{section.4}%
\contentsline {subsection}{\numberline {4.1}Production}{9}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}di-Higgs Production}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Decay}{9}{subsection.4.3}%
