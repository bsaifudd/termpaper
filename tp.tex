\documentclass{article}
\usepackage[a4paper, portrait, margin=1.0in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage{helvet}
\usepackage{etoolbox}
\usepackage{titlesec}
\usepackage{caption}
\usepackage{booktabs}
\usepackage{xcolor}
\usepackage{array}
\usepackage{subcaption}
\usepackage{latexsym}
\usepackage{pdfpages}
\usepackage{multirow}
\usepackage{xcolor}
\usepackage{amsmath, nccmath}
%\usepackage[fleqn]{amsmath}
\usepackage{slashed}
\usepackage{tikz}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=cyan,
}
\urlstyle{same}
\numberwithin{equation}{section}
\captionsetup[figure]{name=Figure}
\graphicspath{ {./images/} }
\usepackage{scrextend}
\usepackage{fancyhdr}
\usepackage{graphicx}
\newcounter{lemma}
\newtheorem{lemma}{Lemma}
\newcounter{theorem}
\newtheorem{theorem}{Theorem}

\fancypagestyle{plain}{
        \fancyhf{}
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\familydefault}{\sfdefault}
 
}

\makeatletter
\patchcmd{\@maketitle}{\LARGE \@title}{\fontsize{16}{19.2}\selectfont\@title}{}{}
\makeatother

\usepackage{authblk}
\renewcommand\Authfont{\fontsize{12}{10.8}\selectfont}
\renewcommand\Affilfont{\fontsize{11}{10.8}\selectfont}
\renewcommand*{\Authsep}{, }
\renewcommand*{\Authand}{, }
\renewcommand*{\Authands}{, }
%\setlength{\affilsep}{1em}
%\newsavebox\affbox
\author{\textsc{Burhani Taher Saifuddin}}
\affil{Homer L. Dodge Department of Physics \& Astronomy, University of Oklahoma, Norman, Oklahoma.
}
\titlespacing\section{12pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\titlespacing\subsection{12pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\titlespacing\subsubsection{12pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}


\titleformat{\section}{\normalfont\fontsize{10}{15}\bfseries}{\thesection.}{1em}{}
\titleformat{\subsection}{\normalfont\fontsize{10}{15}\bfseries}{\thesubsection.}{1em}{}
\titleformat{\subsubsection}{\normalfont\fontsize{10}{15}\bfseries}{\thesubsubsection.}{1em}{}

\titleformat{\author}{\normalfont\fontsize{10}{15}\bfseries}{\thesection}{1em}{}

\title{\textbf{\huge The Higgs Boson: Production and Decay at the ATLAS and CMS Experiments}\\
        %(Center, Bold, Times New Roman 14, maximum 15 words in english)
        }
\date{}

\begin{document}
\pagestyle{headings}
\newpage
\setcounter{page}{1}
\renewcommand{\thepage}{\arabic{page}}

\captionsetup[figure]{labelfont={bf},labelformat={default},labelsep=period,name={Figure }}      \captionsetup[table]{labelfont={bf},labelformat={default},labelsep=period,name={Table }}
\setlength{\parskip}{0.5em}

\maketitle

\tableofcontents

\section{Introduction}
The Higgs Boson was discovered at CERN jointly by the ATLAS \cite{Aad_2012} and CMS \cite{Chatrchyan_2012} experiments in 2012. It was the last particle to be discovered in the Standard Model of Particle Physics (SM) rendering it complete. The Higgs Boson is a scalar spin-0 particle which is a quantum menifestation of a complex scalar field called the Higgs field that permeates the entire Universe. All the particles in the Standard Model except for the neutrino, acquire mass by interacting with this field. Since its discovery, the properties of the Higgs Boson have been studied extensively--its production cann be studied by measuring the cross-section and its decay rate, by measuring which particles it decays into and how often, or the branching fraction. The coupling of the Higgs Boson to the SM particles and to itself is also a very extensively researched area of study. Knowing these couplings makes testing Beyond the Standard Model (BSM) theories easier. 

This write-up is a review of the papers by Christoph Englert \cite{Englert:2019fku} on Higgs Physics and the properties of the Higgs Boson as summarised by the ATLAS \cite{ATLAS:2022vkf} and the CMS \cite{CMS:2022dwd} experiments. The article includes an introduction to Spontaneous Symmetry Breaking and Higgs mechanism, a brief overview of the ATLAS \cite{ATLAS:2008xda} and CMS \cite{CMS:2008xjf} detectors at LHC-CERN and the properties of the Higgs Boson as studied in the ten years after its discovery.

\section{Electroweak Interactions and Spontaneous Symmetry Breaking}
Consider a Lagrangian for the $\phi^4$ theory,

\begin{equation}
        \label{phi4lag}
        \begin{aligned}
		L = \frac{1}{2} (\partial_{\mu} \phi)^2 - \frac{1}{2} m^{2}\phi^{2} - \frac{\lambda}{4!} \phi^4
        \end{aligned}
\end{equation}

which is invariant under the operation, $\phi \rightarrow -\phi$. Replacing the mass $m^2$ with the negative parameter $-\mu^2$, the potential becomes, 

\begin{equation}
        \label{phi4pot}
        \begin{aligned}
		V(\phi) = -\frac{1}{2}\mu^{2}\phi^{2} + \frac{\lambda}{4!}\phi^{4}
        \end{aligned}
\end{equation}

whose minima is evaluated at $\phi_{min} = \phi_0 = \pm \sqrt{\frac{6}{\mu}} = \pm v$ 

where $v$ is called the vacuum expectation value (vev) of $\phi$. For the system near the positive minima, we define $\phi_0 = v + \sigma(x)$. The Lagrangian then becomes,

\begin{equation}
        \label{phi4nlag}
        \begin{aligned}
		L = \frac{1}{2} (\partial_{\mu} \sigma)^2 - \frac{1}{2} (2 \mu)^{2}\sigma^{2} - \sqrt{\frac{\lambda}{6}}\mu\sigma^3 - \frac{\lambda}{4!}\sigma^{4}
        \end{aligned}
\end{equation}

where terms linear in $\sigma(x)$ are dropped, along with the constants. This Lagrangian now describes a field $\sigma(x)$ of mass $\sqrt{2}\mu$, along with $\sigma^3$ and $\sigma^4$ interactions. As is evident, the symmetry $\phi \rightarrow -\phi$ is broken. This is called spontaneous symmetry breaking \cite{Peskin:1995ev}. 

\subsection{Higgs Mechanism} 

In 1964, Brout and Englert, Higgs, and Guralnik, Hagen and Kibble proposed what is now called The Brout-Englert-Higgs (BEH) mechanism. Further details were later presented by Higgs in 1966 and Kibble in 1967 \cite{CMS:2022dwd}. This mechanism introduces a complex scalar spin-0 field called the Higgs field, with which the standard model particles interact and acquire mass, except for the neutrinos.

The QED Lagrangian is,

\begin{equation}
        \label{qedlag}
        \begin{aligned}
		L_{qed} = L_{em} + L_{dir} + L_{int} = -\frac{1}{4}F^{\mu \nu}F_{\mu \nu} + \bar{\psi}[i\gamma^{\mu}(\partial_{\mu} + ie A_{\mu})] \psi 
        \end{aligned}
\end{equation}

which is gauge invariant under the U(1) local gauge symmetry, $\psi(x) \rightarrow \psi^{\prime}(x) = e^{-ie\alpha(x)} \psi(x)$ and $A_{\mu}(x) \rightarrow A^{\prime}_{\mu}(x) = A_{\mu}(x) + \partial_{\mu} \alpha(x)$. This brings about a problem with the $A_{\mu}(x)$ field. The field must describe a photon, which being massless only has two degrees of freedom. Whereas due to gauge symmetry, the $A_{\mu}(x)$ field has four degrees of freedom. To fix the gauge, we us Faddeev-Popov procedure and add a gauge fixing term to the Lagrangian.

\begin{equation}
        \label{gflag}
        \begin{aligned}
		L_{gf} = -\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2
        \end{aligned}
\end{equation}

The full Lagrangian is,

\begin{equation}
        \label{qedgflag}
        \begin{aligned}
		L_{qed} = L_{em} + L_{dir} + L_{int} + L_{gf} = -\frac{1}{4}F^{\mu \nu}F_{\mu \nu} + \bar{\psi}[i\gamma^{\mu}(\partial_{\mu} + ie A_{\mu})] \psi -\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2 
        \end{aligned}
\end{equation}

as we can see, adding a mass term to the above Lagrangian will break the U(1) gauge symmetry. QED has demonstrated exceptional agreement with the experimental data. Writing a similar Lagrangain for the weak interactions is not so straightforward as the weak gauge bosons are massive. 

Peter Higgs came up with a trick to introduce mass terms to the above lagrangian without breaking the U(1) gauge symmetry. The idea is to start with a free theory of massless gauge bosons where the propogator behaves like $\sim i p^{-2}$. Now, introducing a scalar potential into the field with a strength $-i\Sigma = ie^2 \langle\phi\rangle^2$. Dyson-resumming all the 1 particle irreducible (1PI) interactions, we get

\begin{fleqn}[\parindent]
\begin{equation}
        \label{series}
        \begin{split}
		ip^{-2} + ip^{-2} (-i\Sigma)\,i p^{-2} + ip^{-2} (-i\Sigma) \, ip^{-2} (-i\Sigma) \,ip^{-2} + ... = ip^{-2} \left(1 - \frac{\Sigma}{p^2}\right)^{-1} = \frac{i}{p^2 - e^2\langle\phi^2\rangle}
        \end{split}
\end{equation}
\end{fleqn}

Hence, the introduction of a scalar field, shifts the mass pole of the photon field from $p^2 = 0$ to $p^2 = m^{2}_A = e^2\langle\phi^2\rangle$. This ``acquiring'' of mass also introduces a new degree of freedom, the longitudinal polarization of the gauge boson \cite{Englert:2019fku}. Which for large momenta behaves as,

\begin{equation}
        \label{longi}
        \begin{aligned}
		\epsilon^{\mu}(k) \simeq \frac{k^{\mu}}{m_A}
        \end{aligned}
\end{equation}

In order for $L_{em}$ to develop a vev $\langle\phi\rangle = v/\sqrt{2}$, we introduce a complex scalar field, $\phi (x) = \frac{1}{\sqrt{2}} e^{i\chi(x)/v} (v + h(x))$ around the minimum of the potential similar to  eq. (\ref{phi4pot}). Where $h(x)$ is a real-valued field. $L_em$ now becomes, 

\begin{fleqn}[\parindent]
\begin{equation}
        \label{emlag}
        \begin{split}
		L_{em} &= -\frac{1}{4}F^{\mu\nu}F_{\mu\nu} + \frac{e^2 v^2}{2} A_{\mu}A^{\mu} \\
		&+ \frac{1}{2}(\partial_{\mu}h)^2 + \mu^2h^2 + e^2vA_{\mu}A^{\mu}h + \frac{1}{2}e^2A_{\mu}A^{\mu}h^2 \\
		&+\frac{1}{2}(\partial_{\mu}\chi)^2 - evA_{\mu}\partial^{\mu}\chi + ... \\
        \end{split}
\end{equation}
\end{fleqn}

The second term in the first line of eq. (\ref{emlag}) gives a mass to the photon $A_{\mu}$ field. The mass $m_A = ev$ is determined by the strength of the interaction as well as the order parameter; v determines whether the symmetry is ``broken'' or not. The second term in the second line of eq. (\ref{emlag}) gives a mass term $m_h = \sqrt{2}\mu$ to the h field, which is expected. Another consequence of this is that there exists an another massless scalar boson $\chi$ which mixes with the $A_{\mu}$ field proportional to the mass of the gauge field, $m_A$. This is called the Higgs Mechanism. 

\subsection{The Glashow-Weinberg-Salam (GWS) theory of Electroweak Interactions}
In 1967, Weinberg and Salam extended the 1961 work of Glashow of spontaneous symmetry breaking and applied it to unify the electromagnetic and weak interactions using the BEH mechanism \cite{Peskin:1995ev}. The proposed that nature consists of an electroweak symmetry which is spontaneously broken giving masses to the weak W and Z bosons. This also gives mass to the fermions but, with the yukawa interactions. Thus, all the elementary particles acquire mass due to the BEH mechanism. Renormalizability of such a theory was a concern until in 1971, 'tHooft and Veltman showed that it was indeed renormalizable.   

Spontaneously broken symmetry in the SU(2) symmetry group gives three massive gauge bosons. To write a theory for the electromagnetic interactions, we need a massless gauge boson, $\gamma$ whereas for a theory of weak interactions, we need three massive gauge bosons. Therefore, along with an SU(2) we introduce a U(1) symmetry group. The complex scalar fields are assigned a charge $1/2$ under the U(1) symmetry.

\begin{equation}
	\label{compscalar}
	\begin{aligned}
		\phi \rightarrow e^{i\alpha^{a}\tau^{a}} e^{i\frac{\beta}{2}} \phi
	\end{aligned}
\end{equation}

where a = 1,2,3. The potential is, 

\begin{equation}
        \label{potential}
        \begin{aligned}
		\text{V}(\phi) = -\mu^{2}\phi^{\dag}\phi + \lambda(\phi^{\dag}\phi)^2
        \end{aligned}
\end{equation}
giving vev of $\phi$ as,

\begin{equation}
        \label{vev}
        \begin{aligned}
		\langle\phi\rangle = \frac{1}{\sqrt{2}} 
		\begin{pmatrix}
			0 \\
			v
		\end{pmatrix}
        \end{aligned}
\end{equation}

A gauge transformation of the above vev, with $\alpha^{1} = \alpha^{2} = 0$ and $\beta = \alpha^{3}$ leaves the vev invariant. Hence, there exists a massless gauge boson. The covariant derivative of such a theory is,

\begin{equation}
        \label{covder}
        \begin{aligned}
		D_{\mu}\phi = (\partial_{\mu} - i g A^{a}_{\mu}\tau^{a} - i\frac{1}{2} g^{\prime} B_{\mu})\phi
        \end{aligned}
\end{equation}

where $\text{A}^{a}_{\mu}$ and $\text{B}_{\mu}$ are the guage boson eigenstates. $\text{g}$ and $\text{g}^{'}$ are the SU(2) and U(1) couplings respectively and the factor $1/2$ is for the weak hypercharge Y. The kinetic term $\frac{1}{2}| D_{\mu}\phi|^{2}$ evaluated at the vev \ref{vev} gives

\begin{equation}
        \label{kinetic}
        \begin{aligned}
		\Delta L = \frac{v^{2}}{8} \{ g^{2} [(A^{1}_{\mu})^{2} + (A^{2}_{\mu})^{2}] + (g A^{3}_{\mu} - g^{\prime} B_{\mu})^{2} \}
        \end{aligned}
\end{equation}

The three massive gauge bosons are,

\begin{equation}
        \label{mbosons}
        \begin{aligned}
		W^{\pm}_{\mu} = \frac{1}{\sqrt{2}} (A^{1}_{\mu} \mp A^{2}_{\mu}) \\ 
		Z^{0}_{\mu} = \frac{1}{\sqrt{g^{2} + g^{\prime 2}}} (g A^{3}_{\mu} - g^{\prime} B_{\mu})
        \end{aligned}
\end{equation}

with masses $m_W = \frac{g v}{2}$ and $m_Z = \sqrt{g^{2} + g^{\prime 2}}\frac{v}{2}$. The fourth vector boson is orthogonal to $Z^{0}_{\mu}$ and remains massless.

\begin{equation}
        \label{bosons}
        \begin{aligned}
		A_{\mu} = \frac{1}{\sqrt{g^{2} + g^{\prime 2}}} (g^{\prime} A^{3}_{\mu} + g B_{\mu})
        \end{aligned}
\end{equation}

for fermions belonging to the SU(2) representation, the covariant derivative becomes, 

\begin{equation}
        \label{ncovder}
        \begin{aligned}
		D_{\mu} = \partial_{\mu} - \frac{i g}{\sqrt{2}} (W^{+}_{\mu} T^{+} + W^{-}_{\mu} T^{-}) -\frac{i Z_{\mu}}{\sqrt{g^2 + g^{\prime 2}}} (g^2 T^3 - g^{\prime 2} Y) - \frac{i g g^{\prime}}{\sqrt{g^2 + g^{\prime 2}}} A_{\mu} (T^3 + Y)
        \end{aligned}
\end{equation}

where $T^{a} = \frac{\sigma^{a}}{2}$ and $T^{\pm} = T^{1} \pm T^{-}$, $\sigma^{a}$ being the pauli matrices. In terms of the weak mixing angle, $\theta_W$ the above equation becomes,

\begin{equation}
        \label{nncovder}
        \begin{aligned}
		D_{\mu} = \partial_{\mu} - \frac{i g}{\sqrt{2}} (W^{+}_{\mu} T^{+} + W^{-}_{\mu} T^{-}) -\frac{i Z_{\mu}}{\cos\theta_W} (T^3 - Q \sin^{2}\theta_W ) - i e A_{\mu} Q
        \end{aligned}
\end{equation}

where 

\begin{equation}
        \label{sincos}
        \begin{aligned}
		\cos\theta_W = \frac{g}{\sqrt{g^2 + g^{\prime 2}}}, \;\; \sin\theta_W = \frac{g^{\prime}}{\sqrt{g^2 + g^{\prime 2}}}, \;\; e = g\sin\theta_W \;\; \text{and} \;\; Q = T^3 + Y
        \end{aligned}
\end{equation}

where Q is the electric charge. From above, we also get $\cos\theta_W = \frac{m_W}{m_Z}$. 

The W bosons only couple to left-handedfermions via the V-A interaction theory. Hence, we transfor the left-handed fermions as a SU(2) doublet and the right-handed fermions as SU(2) singlets. They are differect representations of the same gauge symmetry group. 

\begin{equation}
        \label{leftright}
        \begin{aligned}
		E_L = \begin{pmatrix}
			\nu_L \\
			e_L
			\end{pmatrix}
			, \; e_R, \; \nu_R, \; Q_L = \begin{pmatrix}
                        u_L \\
                        d_L
                        \end{pmatrix}, \; u_R, \; d_R
        \end{aligned}
\end{equation}

The lagrangian is given by,
\begin{equation}
        \label{lagr}
        \begin{aligned}
		L = i(\bar{E}_L \slashed{\partial} E_L) + i(\bar{e}_R \slashed{\partial} e_R) + i(\bar{Q}_L \slashed{\partial} Q_L) + i(\bar{u}_R \slashed{\partial} u_R) + i(\bar{d}_R \slashed{\partial} d_R) \\
		+ g(W^{+}_{\mu} J^{\mu +}_W + W^{-}_{\mu} J^{\mu -}_W + Z^{0}_{\mu} J^{\mu}_Z) + e A_{\mu} J^{\mu}_{EM}
        \end{aligned}
\end{equation}

where the currents are,
\begin{fleqn}[\parindent]
\begin{equation}
        \label{curr}
        \begin{split}
		&J^{\mu +}_W = \frac{1}{\sqrt{2}} (\bar{\nu}_L \gamma^{\mu} e_L + \bar{u}_L \gamma^{\mu} d_L) \\
		&J^{\mu -}_W = \frac{1}{\sqrt{2}} (\bar{e}_L \gamma^{\mu} \nu_L + \bar{d}_L \gamma^{\mu} u_L) \\
		&J^{\mu}_Z = \frac{1}{\cos\theta_W} [ \bar{\nu}_L \gamma^{\mu} \left(\frac{1}{2}\right) \nu_L + \bar{e}_L \gamma^{\mu} \left(-\frac{1}{2} + \sin^{2}\theta_W \right) e_L + \bar{e}_R \gamma^{\mu} \left(\sin^{2}\theta_W \right) e_R + \bar{u}_L \gamma^{\mu} \left(\frac{1}{2} - \frac{2}{3}\sin^{2}\theta_W \right) u_L \\
		 &+ \bar{u}_R \gamma^{\mu} \left(-\frac{2}{3}\sin^{2}\theta_W \right) u_R + \bar{d}_L \gamma^{\mu} \left(-\frac{1}{2} + \frac{2}{3}\sin^{2}\theta_W \right) d_L + \bar{d}_R \gamma^{\mu} \left(\frac{1}{3}\sin^{2}\theta_W \right) d_R ] \\ 
        \end{split}
\end{equation}
\end{fleqn}

the W bosons do not couple to $\nu_R$ so have not written its term. Also, since the left- and right- handed fermions belong to different representations of the symmetry group, they are thought of as distinct particles. Hence, we cannot write a mass term as,

\begin{equation}
        \label{fermass}
        \begin{aligned}
		\Delta L = -m_e ( \bar{e}_L e_R + \bar{e}_R e_L)  
        \end{aligned}
\end{equation}

because it is not gauge invariant. Fermion masses in the GWS theory are written using the Yukawa coupling. 

\begin{equation}
        \label{fermasses}
        \begin{aligned}
		\Delta L_e = -\lambda_e \bar{E}_L \phi e_R - \lambda_e \bar{e}_R \phi^{\dag} e_L + h.c. 
        \end{aligned}
\end{equation}

$\lambda_e$ is a new dimensionless constant. $\phi$ acquiring the vev gives the mass term of the electron, $m_e = \frac{\lambda_e v}{\sqrt{2}}$. Similarly we can write for the quarks,

\begin{equation}
        \label{qmasses}
        \begin{aligned}
		\Delta L_q = -\lambda_d \bar{Q}_L \phi d_R - \lambda_u \epsilon^{a b} \bar{Q}_{La} \phi^{\dag}_{b} u_R + h.c. 
        \end{aligned}
\end{equation}

with masses $m_u = \frac{\lambda_u v}{\sqrt{2}}$ and $m_d = \frac{\lambda_d v}{\sqrt{2}}$ for the up- and down- type quarks repsectively.

\subsection{The Higgs Boson}
Working in the unitarity gauge, we can write the complex scalar field $\phi$as,

\begin{equation}
        \label{hvev}
        \begin{aligned}
		\phi(x) = U(x) \frac{1}{\sqrt{2}} 
                \begin{pmatrix}
                        0 \\
			v + h(x)
                \end{pmatrix}
        \end{aligned}
\end{equation}

where $h(x)$ is a fluctuating real-valued field with $\langle h(x)\rangle = 0$. The usual spinor is acted upon by a general SU(2) gauge transformation U(x) to produce the most general complex-values two-component spinor. Eliminating U(x) from the lagrangian by performing a gauge transformation leaves the $\phi$ with only one physical degree of freedom.

\begin{equation}
        \label{hlag}
        \begin{aligned}
		L \rightarrow |D_{\mu}\phi|^2 + \mu^{2}\phi^{\dag}\phi + \lambda(\phi^{\dag}\phi)^2
        \end{aligned}
\end{equation}
where the potential has a minima at $v = \sqrt{\frac{\mu^2}{\lambda}}$. Evaluating the potential at h gives,
\begin{equation}
        \label{hpot}
        \begin{aligned}
		L_V = -\mu^{2}h^2 - \lambda v h^3 - \frac{1}{4} \lambda h^4 \\
		L_V = - \frac{1}{2} m^{2}_h h^2 - \sqrt{\frac{\lambda}{2}} m_h h^3 - \frac{1}{4} \lambda h^4 
        \end{aligned}
\end{equation}
where $m_h = \sqrt{2} \mu = \sqrt{\frac{\lambda}{2}} v$ is the scalar particle arising from the $h(x)$ field. This is precisely the Higgs Boson.

The kinetic terms of the lagrangian give,

\begin{equation}
        \label{hkin}
        \begin{aligned}
		L_{W,Z} = - \frac{1}{2} (\partial_{\mu} h)^2 + [m^2_{W} W^{\mu +} W^{-}_{\mu} + \frac{1}{2} m^2_{Z} Z^{\mu} Z_{\mu} ] \left(1 + \frac{h}{v}\right)^2
        \end{aligned}
\end{equation}
which indicates that the gauge bosons ``couple'' to the Higgs. This Higgs coupling with the gauge bosons is proportional to the gauging of the symmetry, while the terms proportional to v gives the Higgs self-coupling. Similarly, in the unitarity gauge the fermions too ``couple'' to the Higgs boson.

\begin{equation}
        \label{hkinf}
        \begin{aligned}
		L_f = - m_f f \bar{f} \left(1 + \frac{h}{v}\right)^2
        \end{aligned}
\end{equation}

\section{The Large Hadron Collider (LHC)}

Construction of a new particle accelerator began at the site of The Large Electron-Positron (LEP) collider in 1994. LHC is designed to accelerate protons at 7 terraelectronvolts by electric fields generated in superconducting radio-frequency cavities. The proton beams are guided by strong magnetic fields of upto 8.3 T, by superconducting dipole magnets in vacuum. The LHC rings are organised to produce upto 2,800 bunches comprising of $10^{11}$ protons per bunch. These are seperated by 25 ns each providing a bunch crossing rate of 32 MHz. The latest Run 2 data increased this rate to up to 2 GHz. The protons collide at the center of four experiments the ATLAS, ALICE, CMS and LHCb.

\subsection{The ATLAS Detector}
The ``A Toroidol LHC ApparatuS'' (ATLAS) detector \cite{ATLAS:2008xda} is a multipurpose particle detector at the Large Hadron Collider (LHC) at CERN. It consists of an inner detector, electromagnetic and hadron calorimeters, muon spectrometer and a large system of superconducting air-core toroidal magnets. 

It is cylindrical $(r,\phi)$ in its design with the z-axis pointing along the beam axis while the x- and y- axes point from the point of interaction to the center of the LHC ring and upwards respectively.

\begin{figure}[h]
\centering
        \includegraphics[width=1.0\textwidth]{images/atlasdet.png}
	\caption{A computer generated view of the ATLAS detector. It is 46m long, 25m in diameter and weighs 7,000 tonnes. It is the biggest particle detector ever built. Image taken from \href{https://cds.cern.ch/record/1095924?ln=en}{\textsc{atlas}}}
        \label{atlasdet}
\end{figure}

\subsubsection{Inner Detector}
It is immersed in a 2-T magnetic field parallel to the beam axis and is designed to measure the direction, momentum and charge of the particles. It provides a pseudorapidity ($\eta = -\ln(\tan(\theta/2))$) range of $|\eta| < 2.5 $. The Inner Detector is situated very close ($3.3 $cm) to the interaction point with the first hit registered in the insertable B-layer (IBL) of the Pixel Detector which consists of four layers of silicon pixel detectors. This is then followed by the Semiconductor Tracker (SCT) which surrounds the Pixel Detector and is designed such that each particle hits at least four layers of the silicon pixel detectors. SCT is used to reconstruct the tracks of the charged particles upto a precision of 25 $\mu$m. Finally, the particle registers a hit in the Transition Radiation Tracker (TRT). The TRT can identify electrons based on the fraction of hits that it should register beyond an energy threshold. Te TRT instead of silicon is made of 300,000 thin-walled drift tubes or ``straws''. When the charged particle traverse through these ``straws'', they ionise the gas and produce a detectable electrical signal, which is used in the reconstruction of tracks and the identification of the particle. 

\subsubsection{Calorimeters}
The Calorimeters are desgigned to measure the energy of the particles and to completely absorb them within the detector as they traverse. They cover a pseudorapidity range of $|\eta| < 4.9$ where the region within  $|\eta| < 3.2$ is provided with the Liquid Argon (LAr) electromagnetic calorimeter, with an additional thin layer of LAr covering $|\eta| < 1.8$ coverage to correct for energy loss in the central region. The argon is kept in the liquid state at $-184^o$C and is designed to measure the energy of electrons, photons and hadrons. These particles ionise the liquid argon sandwiched within layers of tungsten, copper or lead which then produce an electrical signal that can be measured.  

The LAr calorimeter is surrounded by the hadronic calorimeter which is made of steel and scintillating tiles. Particles hitting steel layers create a shower of new particles that are absorbed, whereas those hitting the plastic scintillators create a photon shower which is recorded by an electrical signal to be measured later.  

\subsubsection{Muon Spectrometer}
Muons escape the inner detector and the calorimeters undetected and thus have a seperate system of different triggers to identify their presence. The Thin Gap chambers (TGC) and the Resistive Plate chambers (RPC) measure the 2nd coordinate at the end of the detector and the central region respectively. Cathode Strip chambers (CSC) measure the rpecision of the coordinates of the muons at the ends of the chamber while the Monitored Drift Tubes (MDT) measure the curve of the tracks. The entire trigger system covers a pseudorapidity range of $|\eta| < 2.4$.

\subsubsection{Magnet System}
The Central Solenoid Magnet surrounds the inner detector and uses a strong 2-T magnetic field to curve the charged particles to measure their momentum. The Toroidal Magnets use a strong 3.5-T magnetic field to curve the muons to measure their momentum.

\subsection{The CMS Detector}
The ``Compact Muon Solenoid'' (CMS) detector \cite{CMS:2008xjf} is a general-purpose detector desined to identify electrons, muons, photons and hadrons along with measuring their properties like momentum and energy. It has four important components, inner tracking system, the electromagnetic (ECAL) and hadronic calrorimeters (HCAL), and the muon tracking system. The detector is nearly hermetic in design and is equipped with triggers to select only the interesting interactions `events'. 

\subsubsection{Inner Tracker}
The inner tracking system measures the momentum and charge of the particles emerging from the interaction vertex. These particles traverse through the pixel detector configured with four cylindrical layers of silicon sensors in the barrel region, and three disks in the endcap region. The pixel detector is surrounded by layers of silicon sensors to measure the tracks of the particles upto an accuracy of $15 \mu m$. The geometric coverage of the inner tracker goes down to $9^o$ from the beamline.

\begin{figure}[h]
\centering
        \includegraphics[width=1.0\textwidth]{images/cmsdet.png}
	\caption{A computer generated view of the CMS detector. The detector weighs 14,000 tonnes, is 21m long and 15m high and is equipped with a strong 3.8 T magnetic field. Image taken from \href{https://home.cern/resources/image/experiments/cms-images-gallery}{\textsc{cms}}}
        \label{cmsdet}
\end{figure}

\subsubsection{The Electromagnetic Calorimeter}

The ECAL identifies and measures the properties of electrons and charged hadrons. It is made of lead tungsten scintillating detectors which measure the energy of the incoming particles by producing an electromagnetic shower. The geometric coverage of the ECAL goes down to $6^o$ from the beamline.

\subsubsection{The Hadron Calorimeter}

The HCAL is made of sandwiched thick brass absorber plates and scintillator plates. These plates create hadron showers of the incoming particles which later decay into photons that are detected by the phototubes in the detector. The geometric coverage of the detector goes down to $6^o$ from the beamline. The very forward calorimeter augments this geometric coverage by about $0.75^o$ from the beamline.

\subsubsection{Muon System}

Muons are not absorbed by the electromagnetic or the hadron calorimeters. The muon system consists of gas-ionization chambers which measure the momentum of muons. These chambers are designed to also make a `track' of the muons to be further analysed. The geometric coverage of the muon system is about $10^o$ from the beam line.

\section{Properties of the Higgs Boson}

\subsection{Production}
The most dominant process that accounts for about $87\%$ of the Higgs boson production is the gluon-gluon fusion (ggF) process, where two gluons from the protons fuse together to form a fermion triangle loop which then produces a Higgs. The second most dominant process is the vector boson fusion (VBF), which accounts for about $7\%$ of the Higgs boson production. Here, two quarks from the colliding protons produce two weak bosons which then fuse together to produce a Higgs. Other modes are associated Higgs boson production, where along with the Higgs, other particles are also produced. The most common ones are Vector-Higgs (VH) where V=Z,W $4\%$ called the ``Higgsstrahlung'', Higgs with a pair of top quarks ($t\bar{t}H$) $1\%$, Higgs with a pair of bottom quarks ($b\bar{b}H$) $1\%$ and Higgs with a single top (tH) $0.05\%$. Production of a Higgs along with other lighter quark pairs is very small and extremely hard to detect due to detector limitations, see Figure \ref{prod}. 

\begin{figure}[h]
\centering
        \includegraphics[width=1.0\textwidth]{images/Production.png}
        \caption{Higgs boson production through ggF (a), VBF (c), associated production with a pair of top-quarks ($t\bar{t}H$) or bottom-quarks ($b\bar{b}H$) (b), a W/Z boson (d) and a single top (e,f). The coupling modifiers $\kappa$ are labelled for each coupling.}
        \label{prod}
\end{figure}

We categorise these modes of production into events which have distinct signatures. That is, events where a Higgs is produced with two high transverse momentum ($p_T$) jets are categorised as VBF, or as VH if along with jets, there are also leptons and missing transverse energy ($E^{miss}_T$) in the final state, or as $t\bar{t}H$ or $tH$ if the jets are all b-tagged, otherwise the events are categorised as ggF.

\subsection{di-Higgs Production}

One of the most important property of the Higgs boson is its self coupling, $\lambda$. The Standard Model predicts several production modes of two Higgs bosons. The leading production mode comes from ggH where two gluons from each of the two colliding protons fuse to give a fermion loop. The triangle loop produces a Higgs which then converts to two Higgs bosons. The other is a square fermion loop where two Higgs bosons are produces without a virtual Higgs as in the first case. The other modes are from VBF where two quarks interact to produce virtual bosons which then produce a Higgs. There are three ways, first when two quarks produce a pair of virtual bosons which then produce a virtual Higgs which eventually produces two Higgs bosons. Second when the two virtual bosons themselves produce two Higgs bosons without a virtual Higgs and the third when the two virtual bosons exchange another virtual boson to give two Higgs bosons, see Figure \ref{diprod}.  

\begin{figure}[h]
\centering
        \includegraphics[width=1.0\textwidth]{images/diprod.png}
	\caption{Higgs boson pair production through ggH (k,l) and VBF (m,n,o). The coupling modifiers $\kappa$ are labelled for each different coupling.}
        \label{diprod}
\end{figure}

\begin{figure}[h]
\centering
        \includegraphics[width=1.0\textwidth]{images/Decay.png}
        \caption{Higgs boson decays into a pair of W or Z bosons (g), a pair of fermions, mainly top/bottom quarks and the $\tau$-lepton (h) and a pair of photons or Z$\gamma$ (i,j). The coupling modifiers $\kappa$ are labelled for each different coupling.}
        \label{decay}
\end{figure}


\subsection{Decay}

The Higgs boson decays most dominantly into a pair of b-quarks with a branching fraction/probability of about $58\%$, $22\%$ into a pair of W-bosons, $6\%$ into a pair of $\tau$-leptons, $3\%$ into a pair of Z-bosons, $3\%$ into a pair of c-quarks, $0.2\%$ into a pair of photons, $0.2\%$ into a photon and a Z, and $0.02\%$ into a pair of $\mu$-leptons. There is a possibility of Higgs boson to decay into BSM particles but there is no evidence of it yet. These decay modes are called invisible Higgs decays and could account for about $1\%$ of the overall branching fraction, see Figure \ref{decay}.     

\bibliographystyle{plain}
\bibliography{ref}
\end{document}

